package ma.ensate.projetgestiondutemps.repositories;

import ma.ensate.projetgestiondutemps.entities.TempsTravail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TempsTravailRepository extends JpaRepository<TempsTravail, Long> {
}
