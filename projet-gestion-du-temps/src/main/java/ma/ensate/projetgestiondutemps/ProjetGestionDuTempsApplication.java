package ma.ensate.projetgestiondutemps;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetGestionDuTempsApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjetGestionDuTempsApplication.class, args);
    }

}
