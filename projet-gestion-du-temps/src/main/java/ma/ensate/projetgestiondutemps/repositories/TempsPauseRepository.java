package ma.ensate.projetgestiondutemps.repositories;

import ma.ensate.projetgestiondutemps.entities.TempsPause;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TempsPauseRepository extends JpaRepository<TempsPause, Long> {
}
