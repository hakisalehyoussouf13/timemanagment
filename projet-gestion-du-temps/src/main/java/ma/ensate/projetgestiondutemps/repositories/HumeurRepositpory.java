package ma.ensate.projetgestiondutemps.repositories;

import ma.ensate.projetgestiondutemps.entities.Humeur;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HumeurRepositpory extends JpaRepository<Humeur, Long> {
}
