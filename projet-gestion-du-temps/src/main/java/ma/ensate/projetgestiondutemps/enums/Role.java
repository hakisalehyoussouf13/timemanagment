package ma.ensate.projetgestiondutemps.enums;

public enum Role {
    USER,
    ADMIN
}
