package ma.ensate.projetgestiondutemps.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;




@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Humeur {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String randomToken;
    private String humeur;
    private Date date;

}
